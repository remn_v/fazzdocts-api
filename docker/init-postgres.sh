#!/usr/bin/env bash
echo "Creating database..."

echo "CREATE DATABASE ${POSTGRES_DB} ENCODING 'UTF-8';" | docker exec -i ${POSTGRES_HOST} psql -U postgres
echo "\l" | docker exec -i ${POSTGRES_HOST} psql -U postgres

echo "Database created."
