import { Sequelize } from 'sequelize-typescript';
import { Doctor } from '../doctors/models/doctor.entity';
import { Hospital } from '../hospitals/models/hospital.entity';
import { Patient } from '../patients/models/patient.entity';
import { configService } from "../lib/env"

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize(configService.getDbConfig());
      sequelize.addModels([Doctor, Hospital, Patient]);
      await sequelize.sync(configService.getDbSyncConfig());
      await sequelize.connectionManager;
      return sequelize;
    },
  },
];
