import { Int, Field, InputType, ID } from 'type-graphql';

@InputType()
export class PatientInput {
  @Field(() => ID, { nullable: true })
  readonly patientId?: number;

  @Field()
  readonly isActive: boolean;

  // @Field()
  // readonly createdAt: Date;

  // @Field()
  // readonly updatedAt: Date;

  @Field()
  readonly patientCode: string;

  @Field()
  readonly patientFirstname: string;

  @Field()
  readonly patientLastname: string;

  @Field()
  readonly patientDob: Date;

  @Field()
  readonly patientAge: number;

  @Field()
  readonly patientNationality: string;

  @Field()
  readonly patientSex: string;

  @Field()
  readonly patientAddress: string;

  @Field()
  readonly patientOccupation: string;

  @Field()
  readonly patientTelnumber: string;

  @Field()
  readonly patientBloodtype: string;

  @Field()
  readonly patientHeight: number;

  @Field()
  readonly patientWeight: number;

}
