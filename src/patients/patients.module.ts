import { Module } from '@nestjs/common';
import { PatientsService } from './controllers/patients.service';
import { patientsProviders } from './models/patients.providers';
import { DatabaseModule } from '../database/database.module';
import { PatientsResolver } from './controllers/patients.resolver';
@Module({
  imports: [DatabaseModule],
  providers: [PatientsService, PatientsResolver, ...patientsProviders],
})
export class PatientModule { }
