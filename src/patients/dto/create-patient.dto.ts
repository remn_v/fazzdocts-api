import { Field, ObjectType, ID, Int } from 'type-graphql';

@ObjectType()
export class CreatePatientDto {
  @Field(() => ID)
  patientId: number;

  @Field()
  isActive: boolean;

  // @Field()
  // createdAt: Date;

  // @Field()
  // updatedAt: Date;

  @Field()
  readonly patientCode: string;

  @Field()
  readonly patientFirstname: string;

  @Field()
  readonly patientLastname: string;

  @Field()
  readonly patientDob: Date;

  @Field()
  readonly patientAge: number;

  @Field()
  readonly patientNationality: string;

  @Field()
  readonly patientSex: string;

  @Field()
  readonly patientAddress: string;

  @Field()
  readonly patientOccupation: string;

  @Field()
  readonly patientTelnumber: string;

  @Field()
  readonly patientBloodtype: string;

  @Field()
  readonly patientHeight: number;

  @Field()
  readonly patientWeight: number;

}
