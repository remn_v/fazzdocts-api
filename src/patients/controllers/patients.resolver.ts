

import { Resolver, Args, Query, Mutation } from '@nestjs/graphql';
import { PatientsService } from './patients.service';
import { CreatePatientDto } from '../dto/create-patient.dto';
import { PatientInput } from '../inputs/patient.input';
import { Int } from 'type-graphql';

@Resolver()
export class PatientsResolver {
  constructor(private readonly patientsService: PatientsService) { }

  @Query(() => String)
  async hello(@Args({ name: 'name', type: () => String }) name: string) {
    return `Hello => ${name}`;
  }
  @Query(() => [CreatePatientDto])
  async getPatients() {
    return this.patientsService.findAllPatients();
  }

  @Query(() => CreatePatientDto, { nullable: true })
  async getPatientById(@Args({ name: 'id', type: () => Int }) id: number) {
    return this.patientsService.getPatient(id);
  }

  @Mutation(() => CreatePatientDto)
  async createPatient(@Args('input') patient: PatientInput) {
    return this.patientsService.createPatient(patient);
  }

  @Mutation(() => CreatePatientDto)
  async updatePatient(@Args('input') patient: PatientInput) {
    return this.patientsService.update(patient);
  }
}


