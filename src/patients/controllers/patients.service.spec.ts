import * as fs from 'fs';
import * as path from 'path';
import * as faker from 'faker';
const EasyGraphQLTester = require('easygraphql-tester')

describe('PatientsModule (Queries)', () => {
  let tester: any;

  beforeAll(() => {
    const schema: any = fs.readFileSync(path.join(__dirname, '../../../', 'schema.gql'), 'utf8');
    tester = new EasyGraphQLTester(schema);
  });

  it('Valid Patient query', done => {
    const query: string = `
      query getPatients {
        getPatients {
          patientId
        }
      }
    `;
    tester.test(true, query);
    done();
  });
});
