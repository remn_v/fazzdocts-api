import { Injectable, Inject } from '@nestjs/common';
// import { CreatePatientDto } from '../dto/create-patient.dto';
import { Patient } from '../models/patient.entity';
import { PatientInput } from '../inputs/patient.input';

@Injectable()
export class PatientsService {
  constructor(
    @Inject('PATIENTS_REPOSITORY') private readonly PATIENTS_REPOSITORY: typeof Patient,
  ) { }

  async findAllPatients(): Promise<Patient[]> {
    return await this.PATIENTS_REPOSITORY.findAll<Patient>();
  }

  async getPatient(id: number): Promise<Patient> {
    return this.PATIENTS_REPOSITORY.findByPk<Patient>(id);
  }

  async createPatient(patient: PatientInput): Promise<Patient> {
    return await this.PATIENTS_REPOSITORY.create<Patient>(patient);
  }
  async update(patient: PatientInput): Promise<Patient> {
    const { patientCode, patientFirstname, patientLastname, patientDob, patientAge, patientNationality, patientSex, patientAddress, patientOccupation, patientTelnumber, patientBloodtype, patientHeight, patientWeight, patientId } = patient;

    const response = await this.PATIENTS_REPOSITORY.update(
      { patientCode, patientFirstname, patientLastname, patientDob, patientAge, patientNationality, patientSex, patientAddress, patientOccupation, patientTelnumber, patientBloodtype, patientHeight, patientWeight, patientId },
      { where: { patientId }, returning: true },
    );
    const [updated, updatedPatient] = response;
    if (!!updated) {
      return updatedPatient[0].dataValues as Patient;
    }
    return;
  }
}




