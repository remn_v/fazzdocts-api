import { Patient } from './patient.entity';

export const patientsProviders = [
  {
    provide: 'PATIENTS_REPOSITORY',
    useValue: Patient,
  },
];


