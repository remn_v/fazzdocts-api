import { Table, Model, Column, DataType, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { IDefineOptions } from 'sequelize-typescript/lib/interfaces/IDefineOptions';

const tableOptions: IDefineOptions = {
  tableName: 'patients',
} as IDefineOptions;

@Table(tableOptions)
export class Patient extends Model<Patient> {
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  patientId: number;

  @Column
  isActive: boolean;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @Column
  patientCode: string;

  @Column
  patientFirstname: string;

  @Column
  patientLastname: string;

  @Column
  patientDob: Date;

  @Column
  patientAge: number;

  @Column
  patientNationality: string;

  @Column
  patientSex: string;

  @Column(DataType.TEXT)
  patientAddress: string;

  @Column
  patientOccupation: string;

  @Column
  patientTelnumber: string;

  @Column
  patientBloodtype: string;

  // @Column
  // patientPhoto:

  @Column
  patientHeight: number;

  @Column
  patientWeight: number;

}
