import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { configService } from "./lib/env";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(configService.getPort());
}
bootstrap();
