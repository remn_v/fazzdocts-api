import { Field, InputType, ID } from 'type-graphql';

@InputType()
export class HospitalInput {
  @Field(() => ID, { nullable: true })
  readonly hospitalId?: number;

  @Field()
  readonly isActive: boolean;

  @Field()
  readonly hospitalCode: string;

  @Field()
  readonly hospitalName: string;

  @Field()
  readonly hospitalAddress: string;
}
