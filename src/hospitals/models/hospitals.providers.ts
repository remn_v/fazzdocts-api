import { Hospital } from './hospital.entity';

export const hospitalsProviders = [
  {
    provide: 'HOSPITALS_REPOSITORY',
    useValue: Hospital,
  },
];
