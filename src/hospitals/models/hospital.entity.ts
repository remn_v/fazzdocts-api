import { Table, Model, Column, DataType, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { IDefineOptions } from 'sequelize-typescript/lib/interfaces/IDefineOptions';

const tableOptions: IDefineOptions = {
  tableName: 'hospitals',
} as IDefineOptions;

@Table(tableOptions)
export class Hospital extends Model<Hospital> {
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  hospitalId: number;

  @Column
  isActive: boolean;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @Column
  hospitalCode: string;

  @Column
  hospitalName: string;

  @Column(DataType.TEXT)
  hospitalAddress: string;

}
