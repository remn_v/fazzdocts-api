import * as fs from 'fs';
import * as path from 'path';
import * as faker from 'faker';
const EasyGraphQLTester = require('easygraphql-tester')

describe('HospitalsModule (Queries)', () => {
  let tester: any;

  beforeAll(() => {
    const schema: any = fs.readFileSync(path.join(__dirname, '../../../', 'schema.gql'), 'utf8');
    tester = new EasyGraphQLTester(schema);
  });

  it('Valid Hospital query', done => {
    const query: string = `
      query getHospitals {
        getHospitals {
          hospitalId
          isActive
          hospitalCode
          hospitalName
          hospitalAddress
        }
      }
    `;
    tester.test(true, query);
    done();
  });
});
