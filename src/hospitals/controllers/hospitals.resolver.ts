import { Resolver, Args, Query, Mutation } from '@nestjs/graphql';
import { HospitalsService } from './hospitals.service';
import { CreateHospitalDto } from '../dto/create-hospital.dto';
import { HospitalInput } from '../inputs/hospital.input';
import { Int } from 'type-graphql';

@Resolver()
export class HospitalsResolver {
  constructor(private readonly hospitalsService: HospitalsService) { }

  @Query(() => String)
  async hello(@Args({ name: 'name', type: () => String }) name: string) {
    return `Hello => ${name}`;
  }
  @Query(() => [CreateHospitalDto])
  async getHospitals() {
    return this.hospitalsService.findAllHospitals();
  }

  @Query(() => CreateHospitalDto, { nullable: true })
  async getHospitalById(@Args({ name: 'id', type: () => Int }) id: number) {
    return this.hospitalsService.getHospital(id);
  }

  @Mutation(() => CreateHospitalDto)
  async createHospital(@Args('input') hospital: HospitalInput) {
    return this.hospitalsService.createHospital(hospital);
  }

  @Mutation(() => CreateHospitalDto)
  async updateHospital(@Args('input') hospital: HospitalInput) {
    return this.hospitalsService.update(hospital);
  }
}
