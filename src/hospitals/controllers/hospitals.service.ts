import { Injectable, Inject } from '@nestjs/common';
// import { CreateHospitalDto } from '../dto/create-hospital.dto';
import { Hospital } from '../models/hospital.entity';
import { HospitalInput } from '../inputs/hospital.input';

@Injectable()
export class HospitalsService {
  constructor(
    @Inject('HOSPITALS_REPOSITORY') private readonly HOSPITALS_REPOSITORY: typeof Hospital,
  ) { }

  async findAllHospitals(): Promise<Hospital[]> {
    return await this.HOSPITALS_REPOSITORY.findAll<Hospital>();
  }

  async getHospital(id: number): Promise<Hospital> {
    return this.HOSPITALS_REPOSITORY.findByPk<Hospital>(id);
  }

  async createHospital(hospital: HospitalInput): Promise<Hospital> {
    return await this.HOSPITALS_REPOSITORY.create<Hospital>(hospital);
  }
  async update(hospital: HospitalInput): Promise<Hospital> {
    const { hospitalCode, hospitalName, hospitalAddress, hospitalId } = hospital;

    const response = await this.HOSPITALS_REPOSITORY.update(
      { hospitalCode, hospitalName, hospitalAddress, hospitalId },
      { where: { hospitalId }, returning: true },
    );
    const [updated, updateHospital] = response;
    if (!!updated) {
      return updateHospital[0].dataValues as Hospital;
    }
    return;
  }
}
