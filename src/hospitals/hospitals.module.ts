import { Module } from '@nestjs/common';
import { HospitalsService } from './controllers/hospitals.service';
import { hospitalsProviders } from './models/hospitals.providers';
import { DatabaseModule } from '../database/database.module';
import { HospitalsResolver } from './controllers/hospitals.resolver';
@Module({
  imports: [DatabaseModule],
  providers: [HospitalsService, HospitalsResolver, ...hospitalsProviders],
})
export class HospitalsModule { }
