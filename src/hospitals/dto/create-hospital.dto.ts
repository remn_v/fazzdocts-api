import { Field, ObjectType, ID, Int } from 'type-graphql';

@ObjectType()
export class CreateHospitalDto {
  @Field(() => ID)
  hospitalId: number;

  @Field()
  readonly isActive: boolean;

  @Field()
  readonly hospitalCode: string;

  @Field()
  readonly hospitalName: string;

  @Field()
  readonly hospitalAddress: string;

}
