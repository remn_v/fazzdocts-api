import { Int, Field, InputType, ID } from 'type-graphql';

@InputType()
export class DoctorInput {
  @Field(() => ID, { nullable: true })
  readonly doctorId?: number;

  @Field()
  readonly isActive: boolean;

  // @Field()
  // readonly createdAt: Date;

  // @Field()
  // readonly updatedAt: Date;

  @Field()
  readonly doctorCode: string;

  @Field()
  readonly doctorFirstname: string;

  @Field()
  readonly doctorLastname: string;

  @Field()
  readonly doctorDob: Date;

  @Field()
  readonly doctorAge: number;

  @Field()
  readonly doctorNationality: string;

  @Field()
  readonly doctorSex: string;
}
