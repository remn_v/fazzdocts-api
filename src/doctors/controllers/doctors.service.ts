import { Injectable, Inject } from '@nestjs/common';
// import { CreateDoctorDto } from '../dto/create-doctor.dto';
import { Doctor } from '../models/doctor.entity';
import { DoctorInput } from '../inputs/doctor.input';

@Injectable()
export class DoctorsService {
  constructor(
    @Inject('DOCTORS_REPOSITORY') private readonly DOCTORS_REPOSITORY: typeof Doctor,
  ) { }

  async findAllDoctors(): Promise<Doctor[]> {
    return await this.DOCTORS_REPOSITORY.findAll<Doctor>();
  }

  async getDoctor(id: number): Promise<Doctor> {
    return this.DOCTORS_REPOSITORY.findByPk<Doctor>(id);
  }

  async createDoctor(doctor: DoctorInput): Promise<Doctor> {
    return await this.DOCTORS_REPOSITORY.create<Doctor>(doctor);
  }
  async update(doctor: DoctorInput): Promise<Doctor> {
    const { doctorCode, doctorFirstname, doctorLastname, doctorDob, doctorAge, doctorNationality, doctorSex, doctorId } = doctor;

    const response = await this.DOCTORS_REPOSITORY.update(
      { doctorCode, doctorFirstname, doctorLastname, doctorDob, doctorAge, doctorNationality, doctorSex, doctorId },
      { where: { doctorId }, returning: true },
    );
    const [updated, updatedDoctor] = response;
    if (!!updated) {
      return updatedDoctor[0].dataValues as Doctor;
    }
    return;
  }
}
