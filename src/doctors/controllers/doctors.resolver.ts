import { Resolver, Args, Query, Mutation } from '@nestjs/graphql';
import { DoctorsService } from './doctors.service';
import { CreateDoctorDto } from '../dto/create-doctor.dto';
import { DoctorInput } from '../inputs/doctor.input';
import { Int } from 'type-graphql';

@Resolver()
export class DoctorsResolver {
  constructor(private readonly doctorsService: DoctorsService) { }

  @Query(() => String)
  async hello(@Args({ name: 'name', type: () => String }) name: string) {
    return `Hello => ${name}`;
  }
  @Query(() => [CreateDoctorDto])
  async getDoctors() {
    return this.doctorsService.findAllDoctors();
  }

  @Query(() => CreateDoctorDto, { nullable: true })
  async getDoctorById(@Args({ name: 'id', type: () => Int }) id: number) {
    return this.doctorsService.getDoctor(id);
  }

  @Mutation(() => CreateDoctorDto)
  async createDoctor(@Args('input') doctor: DoctorInput) {
    return this.doctorsService.createDoctor(doctor);
  }

  @Mutation(() => CreateDoctorDto)
  async updateDoctor(@Args('input') doctor: DoctorInput) {
    return this.doctorsService.update(doctor);
  }
}
