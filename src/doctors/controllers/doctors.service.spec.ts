import * as fs from 'fs';
import * as path from 'path';
import * as faker from 'faker';
const EasyGraphQLTester = require('easygraphql-tester')

describe('DoctorsModule (Queries)', () => {
  let tester: any;

  beforeAll(() => {
    const schema: any = fs.readFileSync(path.join(__dirname, '../../../', 'schema.gql'), 'utf8');
    tester = new EasyGraphQLTester(schema);
  });

  it('Valid Doctor query', done => {
    const query: string = `
      query getDoctors {
        getDoctors {
          doctorId
        }
      }
    `;
    tester.test(true, query);
    done();
  });
});
