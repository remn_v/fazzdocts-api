import { Module } from '@nestjs/common';
import { DoctorsService } from './controllers/doctors.service';
import { doctorsProviders } from './models/doctors.providers';
import { DatabaseModule } from '../database/database.module';
import { DoctorsResolver } from './controllers/doctors.resolver';
@Module({
  imports: [DatabaseModule],
  providers: [DoctorsService, DoctorsResolver, ...doctorsProviders],
})
export class DoctorsModule { }
