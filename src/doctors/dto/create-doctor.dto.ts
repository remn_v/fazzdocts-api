import { Field, ObjectType, ID, Int } from 'type-graphql';

@ObjectType()
export class CreateDoctorDto {
  @Field(() => ID)
  doctorId: number;

  @Field()
  isActive: boolean;

  // @Field()
  // createdAt: Date;

  // @Field()
  // updatedAt: Date;

  @Field()
  doctorCode: string;

  @Field()
  doctorFirstname: string;

  @Field()
  doctorLastname: string;

  @Field()
  doctorDob: Date;

  @Field()
  doctorAge: number;

  @Field()
  doctorNationality: string;

  @Field()
  doctorSex: string;

}
