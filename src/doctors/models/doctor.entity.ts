import { Table, Model, Column, DataType, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { IDefineOptions } from 'sequelize-typescript/lib/interfaces/IDefineOptions';

const tableOptions: IDefineOptions = {
  tableName: 'doctors',
} as IDefineOptions;

@Table(tableOptions)
export class Doctor extends Model<Doctor> {
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  doctorId: number;

  @Column
  isActive: boolean;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @Column
  doctorCode: string;

  @Column
  doctorFirstname: string;

  @Column
  doctorLastname: string;

  @Column(DataType.TEXT)
  doctorAddress: string;

  @Column
  doctorDob: Date;

  @Column
  doctorAge: number;

  @Column
  doctorNationality: string;

  // @Column
  // patientPhoto:

  @Column
  doctorSex: string;

}
