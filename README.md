![alternativetext](public/fazzdoc-logo.png)

## Description
A fazzdoc project test

## Environment
copy .env.sample to .env and config as you wish  


## Installation with docker
```bash
#run docker
$ docker-compose up
```


## Installation without docker

```bash
$ npm i
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

## Playground
  http://localhost:3000/graphql

## Author
  Rian Priyanto - rian6517@gmail.com  
  Rian Priyanto - 76b900@gmail.com

## License
  Nest is [MIT licensed](LICENSE).

